[![pipeline status](https://gitlab.com/lbaudouin/cv/badges/master/pipeline.svg)](https://gitlab.com/lbaudouin/cv/commits/master)


## Requirements

Latex is required, please visit [this forum](http://tex.stackexchange.com/q/55437) to know about Tex installation.

For Linux user, you can install it from command
```bash
sudo apt install texlive-full
```

_moderncv_ package is required
```bash
sudo apt install texlive-latex-extra
```

_fontawesome_ for icons is available through texlive-fonts-extra
```bash
sudo apt install texlive-fonts-extra
```

_babel_ package for french is required
```bash
sudo apt install texlive-lang-french
```

## Usage

```bash
$ pdflatex cv.tex
```
Or open `cv.tex` in [texmaker](http://www.xm1math.net/texmaker/index_fr.html)

## Use last modercv

In order to display Gitlab link and icon, the last moderncv package need to be used.

You can get it with `git submodule`:

```bash
git submodule init
git submodule update
```

## Known issues

 * Not working on Ubuntu 14.04 -> Use recent ubuntu (>=18.04)
 * Image need to be 1:1 ratio
